from flask import app, jsonify, Response, request
from functools import wraps
import jwt


def has_role(role_name):
    def decorator(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            token = jwt.decode(request.headers.get('Authorization')[7:], 'secret', algorithms = ['HS256'])  
            print(token)
           
            if(token['role'] != role_name ):
                return Response("Forbidden access.", status=403)


            return func(*args, **kwargs)

        return wrapped
    
    return decorator