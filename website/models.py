from datetime import timezone
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

from .helper.serializer import Serializer

from . import db

from flask_login import UserMixin
from sqlalchemy.sql import func


users_workdays = db.Table('users_workdays',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('workday_id', db.Integer, db.ForeignKey('workday.id')),
)

class Role(db.Model):
    __tablename__ = 'role'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(150))

    #OTM - B
    teamMembers = relationship("TeamMember", back_populates= 'role')

class Authority(db.Model):
    __tablename__ ='authority'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(200))

    #OTM - B
    users = relationship('User', back_populates= 'authority')

class User(db.Model, UserMixin):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key = True)
    activated = db.Column(db.Boolean)
    email = db.Column(db.String(100), unique = True)
    nameLastName = db.Column(db.String(150))
    username = db.Column(db.String(150))
    password = db.Column(db.String(150)) 

    type = db.Column(db.String(200))

    authority_id = db.Column(db.Integer, ForeignKey('authority.id'))
    authority = relationship('Authority')




class TeamMember(User):
    __tablename__ = 'team_members'
    __mapper_args__ = {
        'polymorphic_identity': 'teamMember'
    }

    hoursPerWeek = db.Column(db.Float)
    status = db.Column(db.Boolean)

    #ManyToOne
    role_id = db.Column(db.Integer, ForeignKey('role.id'))
    role = relationship('Role', back_populates='teamMembers')

    #OTM
    projects = relationship('Project', back_populates='lead')




class Client(db.Model, Serializer):
    __tablename__ = 'client'
    id = db.Column(db.Integer, primary_key = True)
    address = relationship("Address", back_populates="client", uselist=False)
    name = db.Column(db.String(200))

    #OneToMany - bidirectional
    workdays = relationship("Workday", back_populates="client")

    #OneToMany - bidirectional
    projects = relationship("Project", back_populates="client")

    def serialize(self):
        d = Serializer.serialize(self)
        
        del d['workdays']
        del d['projects']
        print("-------------------------------------")
        
        d['address'] = d['address'].serialize()
        return d
    


class Project(db.Model):
    __tablename__ = 'project'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(200))
    description = db.Column(db.String(1000))
    status = db.Column(db.Boolean)
    archive = db.Column(db.Boolean)

    #ManyToOne
    client_id = db.Column(db.Integer, ForeignKey('client.id'))
    client = relationship('Client', back_populates='projects')


    #ManyToOne
    lead_id = db.Column(db.Integer, ForeignKey('user.id'))
    lead = relationship('TeamMember', back_populates='projects')

    #OTM
    workdays = relationship("Workday", back_populates='project')

    def serialize(self):
        d = Serializer.serialize(self)
        
        del d['workdays']
        del d['client']
        del d['lead']
        return d


class Category(db.Model):
    __tablename__ = 'category' 
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(200))

    #OTM
    workdays = relationship('Workday', back_populates='category')


class Workday(db.Model, Serializer):
    __tablename__ = 'workday'
    id = db.Column(db.Integer, primary_key = True)
    date = db.Column(db.DateTime(timezone=True), default=func.now())
    description = db.Column(db.String(10000))
    time = db.Column(db.Float)
    overTime = db.Column(db.Float)

    #ManyToOne
    client_id = db.Column(db.Integer, ForeignKey('client.id'))
    client = relationship('Client', back_populates='workdays')

    #MTO
    category_id = db.Column(db.Integer, ForeignKey('category.id'))
    category = relationship('Category', back_populates=('workdays'))

    #MTO
    project_id = db.Column(db.Integer, ForeignKey('project.id'))
    project = relationship('Project', back_populates='workdays')


    users = db.relationship('User', secondary = users_workdays)


    def serialize(self):
        d = Serializer.serialize(self)
        del d['users']
        del d['client']
        d['category'] = d['category'].name
        d['project'] = d['project'].name
        return d




class Address(db.Model):
    __tablename__ = 'address'
    id = db.Column(db.Integer, primary_key = True)
    street = db.Column(db.String(200))
    city = db.Column(db.String(200))
    zip_code = db.Column(db.String(200))
    country = db.Column(db.String(200))

    client_id = db.Column(db.Integer, ForeignKey('client.id'))
    client = relationship("Client", back_populates='address')

    def serialize(self):
        d = Serializer.serialize(self)
        del d['client']
        del d['client_id']
        return d
   
class Report(db.Model):
    __tablename__ = "None"
    id = db.Column(db.Integer, primary_key = True)
    client = db.Column(db.String(200))
    date = db.Column(db.DateTime(timezone=True), default=func.now())
    teamMember = db.Column(db.String(200))
    project = db.Column(db.String(200))
    category = db.Column(db.String(200))
    description = db.Column(db.String(200))
    time = db.Column(db.Float)

    def serialize(self):
        d = Serializer.serialize(self)
        return d
