from flask import Blueprint, request, Response,g, jsonify
from flask_expects_json import expects_json
from ..Service import ReportService as rs
import json


BASE_URI = '/reports'
report = Blueprint('report', __name__)


report_schema = {
    'type':'object',
    'properties':{
        'teamMember' : {'type':'string'},
        'client' : {'type':'string'},
        'project' : {'type':'string'},
        'category' : {'type':'string'},
        'startDate' : {'type':'string'},
        'endDate' : {'type':'string'}
        
    },
    'required':[]
}

@report.route(BASE_URI, methods = ['POST'])
@expects_json(report_schema)
def get_report():
    report = rs.get_report(g.data)
    return jsonify(list(map(lambda x: x.serialize(), report)))