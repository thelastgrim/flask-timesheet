from flask import Blueprint, request,g, jsonify
from ..jwt.jwt_manager import has_token
from flask_expects_json import expects_json
from ..Service import ClientService as cs
from ..helper.accesControl import has_role

BASE_URI = '/clients/'
client = Blueprint('client', __name__)

create_schema = {
    'type':'object',
    'properties':{
        'name' : {'type':'string'},
        'address' : {
            'street' : {'type':'string'},
            'city' : {'type':'string'},
            'zip_code' : {'type':'string'},
            'country' : {'type':'string'}
        }
    },
    'required':['address', 'name']
}


@client.route(BASE_URI, methods = ['POST'] )
@has_token
@has_role("ROLE_USER")
@expects_json(create_schema)
def create_client():
    client = cs.create(g.data)
    return jsonify(client.serialize())

@client.route(BASE_URI+"<id>", methods = ['PUT'])
@has_token
@has_role("ROLE_USER")
@expects_json(create_schema)
def update_client(id):
    client = cs.update(g.data, id)
    return jsonify(client.serialize())

@client.route(BASE_URI+"<id>", methods = ['GET'])
@has_token
@has_role('ROLE_USER')
def get_client(id):
    client = cs.get_by_id(id)
    return jsonify(client.serialize())

@client.route(BASE_URI+"pages/<letter>/<term>", methods = ['GET'])
@has_token
@has_role('ROLE_USER')
def get_all_clients_by_letter_pageable(letter, term):

    page = request.headers.get('page', type =int)
    pageSize = request.headers.get('pageSize', type =int)
  
    if (term=='*'):
        clients = cs.find_all_by_letter_pagable(letter, page, pageSize)
    else:
        clients = cs.find_matched_by_letter_pageable(letter, term, page, pageSize)
        
    return jsonify(list(map(lambda x: x.serialize(), clients)))
    
@client.route(BASE_URI+'searched/<term>', methods = ['GET'])   
def get_all_clients(term):
    if (term == '*'):
        clients = cs.get_all()
    else:
        clients = cs.find_if_contains(term)

    return jsonify(list(map(lambda x: x.serialize(), clients)))