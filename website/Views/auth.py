from flask import Blueprint, request
from ..Service import AuthService as aus

auth = Blueprint('auth', __name__)

@auth.route("/login", methods =['POST'])
def login():
    email = request.form.get('email')
    password = request.form.get('password') #.encode('utf8')
    return aus.login(email, password)
    
@auth.route("/logout")
def logout():
    return False

@auth.route("/register", methods=['POST'])
def register():
    email = request.form.get('email')
    password = request.form.get('password')
    username = request.form.get('username')   
    nameLastName = request.form.get('nameLastName')

    return aus.register(email, password, username, nameLastName)

    

    



