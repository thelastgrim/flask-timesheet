from flask import Blueprint, request, Response,g, jsonify

from ..jwt.jwt_manager import has_token
from flask_expects_json import expects_json
from flask_login import current_user
from ..Service import ProjectService as ps
from ..helper.accesControl import has_role

BASE_URI = '/projects'
project = Blueprint('project', __name__)

create_schema = {
    'type':'object',
    'properties':{
        'name' : {'type':'string'},
        'description' : {'type':'string'},
        'status' : {'type': 'boolean'},
        'archive' : {'type': 'boolean'},
        'client' : {'type':'integer'},
        'lead' : {'type':'integer'},
    },
    'required':[]
}


@project.route(BASE_URI, methods = ['POST'])
@has_token
@has_role("ROLE_USER")
@expects_json(create_schema)
def create_project():
    
    project = ps.create(g.data)

    return jsonify(project.serialize())

@project.route(BASE_URI+"/<id>", methods =['PUT'])
@has_token
@has_role("ROLE_USER")
@expects_json(create_schema)
def update_project(id):
    project = ps.update(g.data, id)

    return jsonify(project.serialize())

@project.route(BASE_URI+"/<id>", methods =['DELETE'])
@has_token
@has_role("ROLE_USER")
def delete_project(id):
    ps.delete(id)
    return "Deleted."


@project.route(BASE_URI+"/<id>", methods = ['GET'])
def get_one(id):
    project = ps.get_by_id(id)
    return jsonify(project.serialize())


@project.route(BASE_URI + "/pages/<letter>/<term>", methods = ['GET'])
def get_all_projects_by_letter_pageable(letter, term):
    page = request.headers.get('page', type =int)
    pageSize = request.headers.get('pageSize', type =int)

    if (term=='*'):
        projects = ps.find_all_by_letter_pagable(letter, page, pageSize)
    else:
        projects = ps.find_matched_by_letter_pageable(letter, term, page, pageSize)
        
    return jsonify(list(map(lambda x: x.serialize(), projects)))

@project.route(BASE_URI+'/searched/<term>', methods = ['GET'])   
def get_all_projects(term):
    if (term == '*'):
        projects = ps.get_all()
    else:
        projects = ps.find_if_contains(term)

    return jsonify(list(map(lambda x: x.serialize(), projects)))

    