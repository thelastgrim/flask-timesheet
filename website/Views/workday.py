from flask import Blueprint, request, Response,g, jsonify

from ..models import Workday
from ..jwt.jwt_manager import has_token
from flask_expects_json import expects_json
from flask_login import current_user
from ..Service import WorkdayService as ws
from ..helper.accesControl import has_role


BASE_URI = '/workdays/'
workday = Blueprint('workday', __name__)

create_schema = {
    'type':'object',
    'properties':{
        'date' : {'type':'string'},
        'description' : {'type':'string'},
        'time' : {'type': 'string'},
        'overTime' : {'type': 'string'},
        'client' : {'type':'string'},
        'project' : {'type':'string'},
        'category' : {'type':'string'}
    },
    'required':['time', 'client', 'project', 'category']
}

@workday.route(BASE_URI, methods = ['POST'])
@has_token
@has_role("ROLE_USER")
@expects_json(create_schema)
def create_workday():
    wd = ws.create(g.data, current_user)
    return jsonify(wd.serialize())

@workday.route(BASE_URI+"<id>", methods = ['PUT'])
@has_token
@has_role("ROLE_USER")
@expects_json(create_schema)
def update_workday(id):
    wd = ws.update(g.data, id)
    return jsonify(wd.serialize())


@workday.route(BASE_URI+"<id>", methods = ['DELETE'])
@has_token
@has_role("ROLE_USER")
def delete_workday(id):
    ws.delete(id)
    return Response("Delete "+id, status=200, mimetype='application/json')

@workday.route(BASE_URI+"<date>", methods =['GET'])
@has_token
@has_role("ROLE_USER")
def get_by_date(date):
    wd = ws.get_by_date(date)
    return jsonify(list(map(lambda x: x.serialize(), wd)))

@workday.route(BASE_URI, methods = ['GET'])
@has_token
@has_role("ROLE_USER")
def get_all_for_logged_user():
    wds = ws.get_all_for_logged_user()
    return jsonify(list(map(lambda x:x.serialize(), wds)))



@workday.route(BASE_URI + "/page", methods = ["GET"])
def get_all_pagination():
    page = request.headers.get('page', 1, type =int)
    pageSize = request.headers.get('pageSize', 10, type =int)
    wds = ws.get_all_pageable(page, pageSize)
    return jsonify(list(map(lambda x:x.serialize(), wds)))

