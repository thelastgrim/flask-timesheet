from ..Repository import WorkdayRepository as WorkdayRepo

def create(workday, user):
    return WorkdayRepo.create(workday, user)

def update(workday, id):
    return WorkdayRepo.update(workday, id)

def delete(id):
    WorkdayRepo.delete(id)

def get_by_date(date):
    return WorkdayRepo.read_by_date(date)

def get_all_for_logged_user():
    return WorkdayRepo.read_all_for_logged_user()

def get_all_pageable(page, pageSize):
    return WorkdayRepo.read_all_pageable(page, pageSize)