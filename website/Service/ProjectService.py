from ..Repository import ProjectRepository as ProjectRepo

def get_by_name(name):
    return ProjectRepo.read_by_name(name)

def create(project):
    return ProjectRepo.create(project)

def update(new_project, id):
    return ProjectRepo.update(new_project, id)

def delete(id):
    ProjectRepo.delete(id)

def get_by_id(id):
    return ProjectRepo.read(id)

def find_all_by_letter_pagable(letter, page, page_size):
    return ProjectRepo.read_all_by_first_letter_pageable(letter, page, page_size)

def find_matched_by_letter_pageable(letter, term, page, page_size):
    return ProjectRepo.search_all_by_first_letter_pageable(letter, term, page, page_size)

def get_all():
    return ProjectRepo.read_all()

def find_if_contains(term):
    return ProjectRepo.search(term)