from ..Repository import UserRepository as UserRepo
from ..Repository import AuthorityRepository as AuthorityRepo
from ..models import User
import bcrypt
from ..jwt import jwt_manager
from flask_login import login_user
from flask import session, Response

def login(email, password):
    user = UserRepo.read_by_email(email)
    if user:
        if bcrypt.checkpw(password.encode('utf-8'),user.password.encode('utf-8')):
            token = jwt_manager.generate_token(user)
            login_user(user, remember=True)
            session['user_id'] = user.id
            return Response(token, status=200, mimetype='application/json')
        else:
            return Response('Username or password incorrect', status=400, mimetype='application/json')
    else:
        return Response('Username or password incorrect', status=400, mimetype='application/json')

def register(email, password, username, nameLastName):
    password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8') #mora decode jer postgres driver
                                                                                         #encoduje vec encodovano
    auth = AuthorityRepo.read(1)
    new_user = User(email=email, username=username, password=password, nameLastName=nameLastName, activated=True, authority = auth, type="User")
    if (UserRepo.create(new_user)):
        return Response ("Created.", status=201, mimetype='application/json')
    else:
        return Response ("Email already exists.", status=400, mimetype='application/json')