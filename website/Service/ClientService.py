from ..Repository import ClientRepository as ClientRepo

def create(client):
    return ClientRepo.create(client)

def update(new_client, id):
    return ClientRepo.update(new_client, id)

def get_by_id(id):
     return ClientRepo.read(id)

def get_by_name(name):
    return ClientRepo.read_by_name(name)

def find_all_by_letter_pagable(letter, page, page_size):
    return ClientRepo.read_all_by_first_letter_pageable(letter, page, page_size)

def find_matched_by_letter_pageable(letter, term, page, page_size):
    return ClientRepo.search_all_by_first_letter_pageable(letter, term, page, page_size)

def get_all():
    return ClientRepo.read_all()

def find_if_contains(term):
    return ClientRepo.search(term)