from ..Repository import AuthorityRepository as AuthorityRepo

def get_by_id(id):
    return AuthorityRepo.read(id)