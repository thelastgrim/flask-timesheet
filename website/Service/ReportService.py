
from ..models import Report
from datetime import datetime

from ..Repository import WorkdayRepository as WordayRepo

def get_report(report_params):

    workdays = __to_report(WordayRepo.read_all())

    if(report_params['category']):
        workdays = filter(lambda x: x.category == report_params['category'], workdays)

    if (report_params['client']):
        workdays = filter(lambda x: x.client == report_params['client'], workdays)

    if (report_params['project']):
        workdays = filter(lambda x: x.project == report_params['project'], workdays)

    if (report_params['teamMember']):
        workdays = filter(lambda x: x.teamMember == report_params['teamMember'], workdays)

    if (report_params['startDate']):
        workdays = filter(lambda x: x.date.replace(tzinfo=None) >= datetime.fromisoformat(report_params['startDate']), workdays)

    if (report_params['endDate']):
        workdays = filter(lambda x: x.date.replace(tzinfo=None) <= datetime.fromisoformat(report_params['endDate']), workdays)

    return workdays




def __to_report(workdays):
    reports = []

    for workday in workdays:
        for user in workday.users:
            
            r = Report(client = workday.client.name,
                        date = workday.date,
                        teamMember= user.nameLastName,
                        project = workday.project.name,
                        category = workday.category.name,
                        description = workday.description,
                        time = workday.time)
            reports.append(r)

    return reports
