from ..Repository import UserRepository as UserRepo

def find_by_email(email):
    return UserRepo.read_by_email(email)

def create(user):
    return UserRepo.create(user)

def get_by_id(id):
    return UserRepo.read_by_id(id)