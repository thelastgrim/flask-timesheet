from ..Repository import CategoryRepository as CategoryRepo

def get_by_name(name):
    return CategoryRepo.read_by_name(name)
