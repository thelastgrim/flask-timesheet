from website.models import TeamMember
from ..Repository import TeamMemberRepository as TeamMemberRepo

def get_by_id(id):
    return TeamMemberRepo.read(id)