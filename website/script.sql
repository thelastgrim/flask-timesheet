insert into authority (name) VALUES ('ROLE_USER');
insert into authority (name) VALUES ('ROLE_TEAMMEMBER');

insert into client (name) values ('Zorka Sabac');
insert into client (name) values ('Brčko Softver');
insert into client (name) values ('Brčko Softver 2');
insert into client (name) values ('Brčko Softver 3');
insert into client (name) values ('Brčko Softver 4');
insert into client (name) values ('Brčko Softver 5');
insert into client (name) values ('Brčko Softver 6');
insert into client (name) values ('Brčko Softver 7');
insert into client (name) values ('Brčko Softver 8');
insert into address (city, country, street, zip_code, client_id) values ('Novi Sad', 'Serbia', 'Lasla Gala 2', 21000, 1);
insert into address (city, country, street, zip_code, client_id) values ('Brčko', 'Bosnia and Herzegovina', 'Augusta Šenoe 4', 76000, 2);
insert into address (city, country, street, zip_code, client_id) values ('Brčko', 'Bosnia and Herzegovina', 'Augusta Šenoe 5', 76000, 3);
insert into address (city, country, street, zip_code, client_id) values ('Brčko', 'Bosnia and Herzegovina', 'Augusta Šenoe 6', 76000, 4);
insert into address (city, country, street, zip_code, client_id) values ('Brčko', 'Bosnia and Herzegovina', 'Augusta Šenoe 7', 76000, 5);
insert into address (city, country, street, zip_code, client_id) values ('Brčko', 'Bosnia and Herzegovina', 'Augusta Šenoe 8', 76000, 6);
insert into address (city, country, street, zip_code, client_id) values ('Brčko', 'Bosnia and Herzegovina', 'Augusta Šenoe 9', 76000, 7);
insert into address (city, country, street, zip_code, client_id) values ('Brčko', 'Bosnia and Herzegovina', 'Augusta Šenoe 10', 76000, 8);
insert into address (city, country, street, zip_code, client_id) values ('Brčko', 'Bosnia and Herzegovina', 'Augusta Šenoe 11', 76000, 9);

insert into category (name) values ('Frontend Development');



insert into role (name) values ('Admin');
insert into role (name) values ('Worker');

insert into "user" (type, email, "nameLastName", password, username, activated, authority_id) values ('User','testss@gmail.com', 'Testo Testic', '$2a$12$FnatfoXEiiK4XRP3uWi39uJUAI0AGQfQILNPmA0FZ0TcR.NlS8eF2', 'Belo roblje', 'True',1);
insert into "user" (type, email, "nameLastName", password, username, "hoursPerWeek", role_id, activated, authority_id) values ('TeamMember','t@gmail.com', 'Timic Membric', '$2a$12$Fuop.cNUjAl8b/B5HZ3qXO8LXbrvYohIqXfM.UwwPVHZ3XJ58gQna','Testino',  10, 2, 'True',2);
insert into "user" (type, email, "nameLastName", password, username, "hoursPerWeek", role_id, activated, authority_id) values ('TeamMember','tt@gmail.com', 'Mimic Membric', '$2a$12$Fuop.cNUjAl8b/B5HZ3qXO8LXbrvYohIqXfM.UwwPVHZ3XJ58gQna','Testino2',  10, 2, 'True',2);
insert into "user" (type, email, "nameLastName", password, username, "hoursPerWeek", role_id, activated, authority_id) values ('TeamMember','ttt@gmail.com', 'Aimic Membric', '$2a$12$Fuop.cNUjAl8b/B5HZ3qXO8LXbrvYohIqXfM.UwwPVHZ3XJ58gQna','Testino3',  10, 2, 'True',2);
insert into "user" (type, email, "nameLastName", password, username, "hoursPerWeek", role_id, activated, authority_id) values ('TeamMember','tttt@gmail.com', 'Cimic Membric', '$2a$12$Fuop.cNUjAl8b/B5HZ3qXO8LXbrvYohIqXfM.UwwPVHZ3XJ58gQna','Testino4',  10, 2, 'True',2);


insert into project (name, description, client_id, lead_id) values ('Test Project', 'Project for database testing', 1, 2);
insert into project (name, description, client_id, lead_id) values ('Test Project Ver.2', 'Project for database implementation', 1, 2);
insert into project (name, description, client_id, lead_id) values ('AquaNox Test Project Ver.2', 'Project for database implementation', 1, 2);
insert into project (name, description, client_id, lead_id) values ('AquaNox Test Project Ver.3', 'Project for database implementation', 1, 2);
insert into project (name, description, client_id, lead_id) values ('AquaNox Test Project Ver.4', 'Project for database implementation', 1, 2);
insert into project (name, description, client_id, lead_id) values ('AquaNox Test Project Ver.5', 'Project for database implementation', 1, 2);
insert into project (name, description, client_id, lead_id) values ('AquaNox Test Project Ver.6', 'Project for database implementation', 1, 2);
insert into project (name, description, client_id, lead_id) values ('AquaNox Test Project Ver.7', 'Project for database implementation', 1, 2);
insert into project (name, description, client_id, lead_id) values ('AquaNox Test Project Ver.8', 'Project for database implementation', 1, 2);
insert into project (name, description, client_id, lead_id) values ('AquaNox Test Project Ver.9', 'Project for database implementation', 1, 2);
insert into project (name, description, client_id, lead_id) values ('Xcon Project Ver.2', 'Project for database implementation', 1, 2);

insert into workday (date, description, "overTime", time, category_id, client_id, project_id) values ('2021-10-14 02:00:00.000000', 'I WORKS HARD', 22, 6.5, 1, 1, 1);
insert into workday (date, description, "overTime", time, category_id, client_id, project_id) values ('2021-10-15 02:00:00.000000', 'I WORKS HARD 2d DAY', 22, 8.5	, 1, 1, 1);
insert into workday (date, description, "overTime", time, category_id, client_id, project_id) values ('2021-10-15 02:00:00.000000', 'I WORKS HARD 2nd project', 22, 8.5	, 1, 1, 2);
insert into users_workdays (workday_id, user_id) values (1, 2);
insert into users_workdays (workday_id, user_id) values (3, 2);
insert into users_workdays (workday_id, user_id) values (2, 2)
