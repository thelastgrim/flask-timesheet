from flask import app, jsonify, Response, request
from functools import wraps
import jwt
import datetime

def has_token(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        if not request.headers.get('Authorization'):
            return Response("Missing authorization.",status=401,mimetype='application/json')
        token = request.headers.get('Authorization')[7:]
        if not token:
            return Response("Token not provided.",status=401,mimetype='application/json')
        
        try: 
            data = jwt.decode(token, 'secret', algorithms = ['HS256'])
        except Exception as a:
            print(a)
            return Response('Invalid token.', status=401, mimetype="application/json")
        
        return func(*args, **kwargs)
    return wrapped
      

def generate_token(user):
    token = jwt.encode({
        'user': user.username,
        'role':user.authority.name,
        'exp': datetime.datetime.utcnow()+datetime.timedelta(seconds=3600)
    },
    'secret')
    #app.config['SECRET_KEY'])

    return token