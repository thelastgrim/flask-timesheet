from ..models import Authority

def read(id):
    return Authority.query.get(id)