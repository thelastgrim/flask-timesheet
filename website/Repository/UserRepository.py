from ..models import User
from .. import db

def read_by_id(id):
    return User.query.get(id)

def read_by_email(email):
    return User.query.filter_by(email=email).first()

def create(user):
    new_user = read_by_email(user.email)
    if (new_user):
        print("uso")
        return None
    
    db.session.add(user)
    db.session.commit()

    return user
