from ..models import Category

def read_by_name(name):
    return Category.query.filter_by(name=name).first()