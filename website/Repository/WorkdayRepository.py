from ..models import Workday
from datetime import datetime
from flask import session
from . import ClientRepository as ClientRepo
from . import CategoryRepository as CategoryRepo
from . import ProjectRepository as ProjectRepo
from .. import db

def read(id):
    return Workday.query.get(id)

def read_all():
    return Workday.query.all()

def read_all_pageable(page, pageSize):
    return Workday.query.paginate(page=page, per_page = pageSize).items

def read_by_date(date):
    return Workday.query.filter_by(date = datetime.fromisoformat(date)).all()

def read_all_for_logged_user():
    return Workday.query.filter(Workday.users.any(id = session['user_id'])).all()

def create(workday, user):
    workday = Workday(date = datetime.fromisoformat(workday['date']),
                      description = workday['description'],
                      client = ClientRepo.read_by_name(workday['client']),
                      category = CategoryRepo.read_by_name(workday['category']),
                      project = ProjectRepo.read_by_name(workday['project']),
                      time = workday['time'],
                      overTime = workday['overTime'])

    workday.users.append(user)
    db.session.add(workday)
    db.session.commit()

    return workday

def update(workday, id):

    old_workday = read(id)
    old_workday.description = workday['description']
    old_workday.category = CategoryRepo.read_by_name(workday['category'])
    old_workday.client = ClientRepo.read_by_name(workday['client'])
    old_workday.project = ProjectRepo.read_by_name(workday['project'])
    old_workday.time = workday['time']
    old_workday.overTime = workday['overTime']

    db.session.commit()

    return old_workday

def delete(id):
    workday = read(id)
    db.session.delete(workday)
    db.session.commit()