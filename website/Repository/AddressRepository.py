from ..models import Address

def read(id):
    return Address.query.get(id)