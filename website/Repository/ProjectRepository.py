from ..models import Project
from . import ClientRepository as ClientRepo
from . import TeamMemberRepository as TeamMemberRepo
from .. import db

def create(project):
    client = ClientRepo.read(project['client'])
    lead = TeamMemberRepo.read(project['lead'])
    
    project = Project(name = project['name'],
                      description = project['description'],
                      status = project['status'],
                      archive = project['archive'],
                      client = client,
                      lead = lead)
                    
    
    db.session.add(project)
    db.session.commit()
    return project

def read(id):
    return Project.query.get(id)

def read_all():
    return Project.query.all()

def read_by_name(name):
    return Project.query.filter_by(name = name).first()

def read_all_by_first_letter_pageable(letter, page, page_size):
    return Project.query.filter(Project.name.startswith(letter)).paginate(page=page, per_page=page_size).items

def search(term):
    return Project.query.filter(Project.name.contains(term)).all()

def search_all_by_first_letter_pageable(letter, term, page, page_size):
    return Project.query.filter(Project.name.startswith(letter),
                               Project.name.contains(term)).paginate(page = page, per_page =page_size).items

def update(new_project, id):
    old_project = read(id)

    old_project.name = new_project['name']
    old_project.description = new_project['description']
    old_project.status = new_project['status']
    old_project.archive = new_project['archive']

    if(old_project.client_id != new_project['client']):
        old_project.client = ClientRepo.read(new_project['client'])
    if(old_project.lead_id != new_project['lead']):
        old_project.lead = TeamMemberRepo.read(new_project['lead'])
    
    db.session.commit()

    return old_project

def delete(id):
    project = read(id)
    db.session.delete(project)
    db.session.commit()