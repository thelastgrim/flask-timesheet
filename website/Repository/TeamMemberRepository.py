from ..models import TeamMember

def read(id):
    return TeamMember.query.get(id)