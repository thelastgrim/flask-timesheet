from ..models import Client, Address
from .. import db

#CRUD

def create(client):

    address = Address(street = client['address']['street'],
                      city = client['address']['city'],
                      zip_code = client['address']['zip_code'],
                      country = client['address']['country'])

    client = Client(name = client['name'],
                    address = address)
                    
    db.session.add(client)
    db.session.commit()
    return client

def read(id):
    return Client.query.get(id)

def read_all():
    return Client.query.all()

def read_by_name(name):
    return Client.query.filter_by(name=name).first()

def read_all_by_first_letter_pageable(letter, page, page_size):
    return Client.query.filter(Client.name.startswith(letter)).paginate(page=page, per_page = page_size).items

def search_all_by_first_letter_pageable(letter, term, page, page_size):
    return Client.query.filter(Client.name.startswith(letter),
                               Client.name.contains(term)).paginate(page = page, per_page =page_size).items

def search(term):
    return Client.query.filter(Client.name.contains(term)).all()

def update(new_client, id):
    address = Address(street = new_client['address']['street'],
                      city = new_client['address']['city'],
                      zip_code = new_client['address']['zip_code'],
                      country = new_client['address']['country'])

    old_client = read(id)
    old_client.name = new_client['name']
 
    if(old_client.address.street == address.street and
        old_client.address.city == address.city and
        old_client.address.zip_code == address.zip_code and
        old_client.address.country == address.country):
        pass
    else:
        db.session.delete(old_client.address)
        old_client.address = address
    db.session.commit()

    return old_client


