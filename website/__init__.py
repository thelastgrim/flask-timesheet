from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from sqlalchemy.sql import text
from sqlalchemy import create_engine
import re

db = SQLAlchemy()
DB_NAME = "FlaskTimeSheet"

def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'secret'
    app.config['SQLALCHEMY_DATABASE_URI']=f'postgresql://postgres:admin@localhost/{DB_NAME}'
    db.init_app(app)

   
    from .Views.auth import auth
    from .Views.workday import workday
    from .Views.client import client
    from .Views.project import project
    from .Views.report import report

    app.register_blueprint(auth, url_prefix='/')
    app.register_blueprint(workday, url_prefix='/')
    app.register_blueprint(client, url_prefix = '/')
    app.register_blueprint(project, url_prefix = '/')
    app.register_blueprint(report, url_prefix = '')

    from .models import User

    db.drop_all(app=app)
    db.create_all(app=app)
    engine = create_engine(f'postgresql://postgres:admin@localhost/{DB_NAME}', echo=True)
   
    with open('c:\WORK\FlaskIntro\website\script.sql', encoding='utf8') as file:
        statements = re.split(r';\s*$', file.read(), flags=re.MULTILINE)
        for statement in statements:
            if statement:
                engine.execute(text(statement))

    

    
    login_manager = LoginManager()
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(id):
        return User.query.get(int(id))
        
    
    return app

'''
def create_database(app):
    if not path.exists('website/' + DB_NAME):
        db.create_all(app=app)

        engine = create_engine(f'sqlite:///website/{DB_NAME}', echo=True)
        with open('c:\WORK\FlaskIntro\website\script.sql', encoding='utf8') as file:
            statements = re.split(r';\s*$', file.read(), flags=re.MULTILINE)
            for statement in statements:
                if statement:
                    engine.execute(text(statement))

        print('-------------------------------------------------------------')
        print('Database created!')
        print('-------------------------------------------------------------')
    else:
        print('-------------------------------------------------------------')
        print("Loading existing database")
        print('-------------------------------------------------------------')
'''